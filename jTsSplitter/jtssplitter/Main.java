/*
 * jTsSplitter - java based mpeg2ts splitter.
 * Copyright (C) 2009-2014 Yukikaze
 */
package jtssplitter;

import java.io.File;


/**
 *
 * @author yukikaze
 */
public class Main {

    /**
     * @param args the command line arguments
     *
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String inf;
        String outf;
        int ch = 0;
        inf = "";
        outf = "";
         System.out.println("USAGE : \njava -jar jTsSplitter.jar <Input file> <Output file> [Option ch number]");
         System.out.println("Option ch number is used in CS splitting.\n");
        for (int i = 0; i < args.length; i++) {
            System.out.println(((Integer)i).toString()+":"+args[i]);
        }
        if (args.length > 1) {
            if (args[0].trim().toUpperCase().equals("-ch".toUpperCase())){
                Chlist chl=new Chlist();
                chl.writeCHList(args[1], args[2],-1,-1);
                System.exit(0);
            }else if (args[0].trim().toUpperCase().equals("-bs".toUpperCase())){
                Chlist chl=new Chlist();
                chl.writeCHList(args[1], args[2],-1,290);
                System.exit(0);
            }else if (args[0].trim().toUpperCase().equals("-cs".toUpperCase())){
                Chlist chl=new Chlist();
                chl.writeCHList(args[1], args[2],-1,-1);
                System.exit(0);
            }

            inf = args[0];
            outf = args[1];
            if (args.length > 2) {
                ch = Integer.parseInt(args[2].trim(), 10);
            }
        }
        //inf = "";
        //outf = "";
        Tsfile t1 = new Tsfile();
        File f=new File(inf);
        if (f.length()>1000*1000){
            t1.splitTS_byte(inf, outf, ch);
        }
        System.exit(0);
    }
}
