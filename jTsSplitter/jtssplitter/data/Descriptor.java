/*
 * jTsSplitter - java based mpeg2ts splitter.
 * Copyright (C) 2009-2010 Yukikaze
 */
package jtssplitter.data;

import java.util.ArrayList;
import jtssplitter.calc;
import jtssplitter.data.descriptor.ARIB_Audio_Component_Descriptor;
import jtssplitter.data.descriptor.ARIB_Video_Compornent_Descriptor;

/**
 *
 * @author yukikaze
 */
public class Descriptor {

    private calc cal = new calc();

    public String analyze(String s) {
        StringBuffer sb=new StringBuffer();
        int curp = 0;
        while (s.length() > curp + 16) {
            String adds = "";
            int tag = cal.TSString2Int(s, curp + 0, 8);
            int len = cal.TSString2Int(s, curp + 8, 8) * 8;
            String des_s = s.substring(curp + 16, curp + 16 + len);
            curp = curp + len + 16;
            switch (tag) {
                case 0xc8://ビデオコントロール記述子
                    switch (cal.TSString2Int(des_s, 3, 4)) {
                        case 0:
                            adds = "Video:1080p";
                            break;
                        case 1:
                            adds = "Video:1080i";
                            break;
                        case 2:
                            adds = "Video:720p";
                            break;
                        case 3:
                            adds = "Video:480p";
                            break;
                        case 4:
                            adds = "Video:480i";
                            break;
                        case 5:
                            adds = "Video:240p";
                            break;
                        case 6:
                            adds = "Video:120p";
                            break;
                        case 7:
                            adds = "Video:2160p";
                            break;
                    }
                    break;
                case 0xc4://音声コンポーネント記述子
                    switch (cal.TSString2Int(des_s, 8, 8)) {
                        case 0x01:
                            adds = "Video:120p";
                            break;

                    }
                    break;
            }
            if (adds.length() > 0) {
                sb.append(adds);
            }
        }
        return sb.toString();
    }
    public ArrayList<jtssplitter.data.descriptor.abstract_Descriptor> getDescriptors(String s){
        jtssplitter.calc cal=new calc();
        return getDescriptors(cal.String2Byte(s));
    }
    public ArrayList<jtssplitter.data.descriptor.abstract_Descriptor> getDescriptors(byte[] b){
        int curp = 0;
        ArrayList<jtssplitter.data.descriptor.abstract_Descriptor> ad=new ArrayList<jtssplitter.data.descriptor.abstract_Descriptor>();
        while (b.length > curp + 2) {
            int tag = b[curp]&0xFF;
            int len = b[curp+1]&0xFF;
            byte[] btag=new byte[len];
            System.arraycopy(b,curp+2,btag,0,len);
            curp = curp + len + 2;
            switch (tag) {
                case 0xc8://ビデオコントロール記述子
                    jtssplitter.data.descriptor.ARIB_Video_Compornent_Descriptor avcd=new jtssplitter.data.descriptor.ARIB_Video_Compornent_Descriptor();
                    avcd.analyzeDescriptor(0xc8, btag);
                    ad.add(avcd);
                    break;
                case 0xc4://音声コンポーネント記述子
                    ARIB_Audio_Component_Descriptor aacd=new ARIB_Audio_Component_Descriptor();
                    aacd.analyzeDescriptor(0xC4, btag);
                    ad.add(aacd);
                    break;
            }
        }
        return ad;
    }
}
