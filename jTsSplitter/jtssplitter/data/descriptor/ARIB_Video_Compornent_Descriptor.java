/*
 * jTsSplitter - java based mpeg2ts splitter.
 * Copyright (C) 2009-2010 Yukikaze
 */

package jtssplitter.data.descriptor;

import jtssplitter.data.descriptor.abstract_Descriptor;
/**
 *
 * @author yukikaze
 */
public class ARIB_Video_Compornent_Descriptor extends jtssplitter.data.descriptor.abstract_Descriptor{
    private int Descriptor;
    private boolean still_picture_flag;
    private boolean sequence_end_code_flag;
    private int video_encode_format;

    /**
     * @return the still_picture_flag
     */
    public boolean isStill_picture_flag() {
        return still_picture_flag;
    }

    /**
     * 動画フォーマットが直後から変更されるかどうか
     * @return the sequence_end_code_flag
     */
    public boolean isSequence_end_code_flag() {
        return sequence_end_code_flag;
    }

    /**
     * 動画のフォーマットを示す。
     * @return the video_encode_format
     * 0-1080p
     * 1-1080i
     * 2-720p
     * 3-480p
     * 4-480i
     * 5-240p
     * 6-120p
     * 7-2160p
     */
    public int getVideo_encode_format() {
        return video_encode_format;
    }

    @Override
    public void analyzeDescriptor(int DescriptorTag, byte[] descriptor) {
        Descriptor=DescriptorTag;
        still_picture_flag=((descriptor[0]&0x80)>>7==1);
        sequence_end_code_flag=((descriptor[0]&0x40)>>6==1);
        video_encode_format=(descriptor[0]&0x3C)>>2;
    }

    @Override
    public int getDescriptorTag() {
        return Descriptor;
    }

    @Override
    public void analyzeDescriptor(byte[] descriptor) {
        Descriptor=descriptor[0]&0xFF;
        still_picture_flag=((descriptor[2]&0x80)>>7==1);
        sequence_end_code_flag=((descriptor[2]&0x40)>>6==1);
        video_encode_format=(descriptor[2]&0x3C)>>2;
    }

}
