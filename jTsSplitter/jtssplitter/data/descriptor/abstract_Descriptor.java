/*
 * jTsSplitter - java based mpeg2ts splitter.
 * Copyright (C) 2009-2010 Yukikaze
 */

package jtssplitter.data.descriptor;

/**
 * 記述子記述のための抽象メソッド
 * @author yukikaze
 */
abstract public class abstract_Descriptor {
    /**
     *記述子タグを得る。
     * @return
     */
    abstract public int getDescriptorTag();
    /**
     * 記述子を記述したbyte列を与え、解析する。
     * @param descriptor
     * 記述子本体
     */
    abstract public void analyzeDescriptor(byte[] descriptor);
    /**
     * 記述子の一部を指定した状態で解析させる。
     * @param DescriptorTag
     * 記述子のタグを指定する。
     * @param TagLength
     * タグの長さ
     * @param descriptor
     * 記述子本体
     */
    abstract public void analyzeDescriptor(int DescriptorTag,byte[] descriptor);
}
