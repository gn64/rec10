/*
 * jTsSplitter - java based mpeg2ts splitter.
 * Copyright (C) 2009-2010 Yukikaze
 */

package jtssplitter.data;

/**
 * PMTのデータ。
 * @author Yukikaze
 */
public class PMTData {
    /**
     * プログラムの番号
     * CS、BSだとチャンネル番号になる。
     */
    public int Program_Table;
    /**
     * Streamのタイプを決定する
     */
    public int Stream_Type;
    public int PCR_PID;
    public int Elementary_PID;
}
