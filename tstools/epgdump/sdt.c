// -*- tab-width:4 -*-

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "sdt.h"
#include "ts_ctl.h"

int parseSDThead(unsigned char *data, SDThead *h) {
	int boff = 0;

	memset(h, 0, sizeof(SDThead));

	boff = 0;
	h->table_id = getBit(data, &boff, 8);
	h->section_syntax_indicator = getBit(data, &boff, 1);
	h->reserved_future_use1 = getBit(data, &boff, 1);
	h->reserved1 = getBit(data, &boff, 2);
	h->section_length = getBit(data, &boff, 12);
	h->transport_stream_id = getBit(data, &boff, 16);
	h->reserved2 = getBit(data, &boff, 2);
	h->version_number = getBit(data, &boff, 5);
	h->current_next_indicator = getBit(data, &boff, 1);
	h->section_number = getBit(data, &boff, 8);
	h->last_section_number = getBit(data, &boff, 8);
	h->original_network_id = getBit(data, &boff, 16);
	h->reserved_future_use2 = getBit(data, &boff, 8);

	return 11;
}

int parseSDTbody(unsigned char *data, SDTbody *b) {
	int boff = 0;

	memset(b, 0, sizeof(SDTbody));

	b->service_id = getBit(data, &boff, 16);
	b->reserved_future_use1 = getBit(data, &boff, 3);
	b->EIT_user_defined_flags = getBit(data, &boff, 3);
	b->EIT_schedule_flag = getBit(data, &boff, 1);
	b->EIT_present_following_flag = getBit(data, &boff, 1);
	b->running_status = getBit(data, &boff, 3);
	b->free_CA_mode = getBit(data, &boff, 1);
	b->descriptors_loop_length = getBit(data, &boff, 12);

	return 5;
}

int parseSVCdesc(unsigned char *data, SVCdesc *desc) {
//0x48のサービス記述子、放送局の名前などが入っているよう
	int boff = 0;

	memset(desc, 0, sizeof(SVCdesc));
	desc->descriptor_tag = getBit(data, &boff, 8);
	desc->descriptor_length = getBit(data, &boff, 8);
	desc->service_type = getBit(data, &boff, 8);
	desc->service_provider_name_length = getBit(data, &boff, 8);
	getStr(desc->service_provider_name, data, &boff, desc->service_provider_name_length);
	desc->service_name_length = getBit(data, &boff, 8);
	getStr(desc->service_name, data, &boff, desc->service_name_length);

	return desc->descriptor_length + 2;
}

int parseLOGdesc(unsigned char *data, LOGdesc *desc) {
//0xC1のデジタルコピー制御記述子
//0xCFのロゴ伝送記述子
	int boff = 0;

	memset(desc, 0, sizeof(LOGdesc));
	desc->descriptor_tag = getBit(data, &boff, 8);
	desc->descriptor_length = getBit(data, &boff, 8);
	desc->logo_transmission_type = getBit(data, &boff, 8);
	if ( desc->logo_transmission_type == 0x01 ) {
		desc->reserved_future_use1 = getBit(data, &boff, 7);
		desc->logo_id = getBit(data, &boff, 9);
		desc->reserved_future_use2 = getBit(data, &boff, 4);
		desc->logo_version = getBit(data, &boff, 12);
		desc->download_data_id = getBit(data, &boff, 16);
	}
	else if ( desc->logo_transmission_type == 0x02 ) {
		desc->reserved_future_use1 = getBit(data, &boff, 7);
		desc->logo_id = getBit(data, &boff, 9);
	}
	else if ( desc->logo_transmission_type == 0x03 ) {
		memcpy(desc->logo_char, data + boff / 8, desc->descriptor_length);
		// getStr(desc->logo_char, data, &boff, desc->descriptor_length);
	}

	return desc->descriptor_length + 2;
}

int serachid(SVT_CONTROL *top, int service_id)
{
	SVT_CONTROL	*cur = top ;
	while(cur != NULL){
		if(cur->event_id == service_id){
			return 1 ;
		}
		cur = cur->next ;
	}
	return 0 ;
}

void enqueue_sdt(SVT_CONTROL *top, SVT_CONTROL *sdtptr)
{
	SVT_CONTROL	*cur ;

	if(top->next == NULL){
		top->next = sdtptr ;
		top->prev = top ;
		return ;
	}
	cur = top->next ;
	while(cur != NULL){
		if(sdtptr->event_id < cur->event_id){
			if(cur->prev != NULL){
				cur->prev->next = sdtptr ;
				sdtptr->prev = cur->prev ;
			}
			cur->prev = sdtptr ;
			sdtptr->next = cur ;
			return ;
		}
		if(cur->next == NULL){
			cur->next = sdtptr ;
			sdtptr->prev = cur ;
			return ;
		}
		cur = cur->next ;
	}
	return ;

}

void dumpSDT(unsigned char *ptr, SVT_CONTROL *top,STATION **station, int * station_count,char *ontvheader)
{

	SDThead  sdth;
	SDTbody  sdtb;
	SVCdesc  desc;
	LOGdesc  logd;
	SVT_CONTROL	*svtptr ;
	STATION * pStation = *station;
	int		rc ;

	int len = 0;
	int loop_len = 0;
	int desc_len = 0;
	char sid[80];
	int stationi = *station_count;
	int i = 0;

	/* SDT */
	len = parseSDThead(ptr, &sdth); 
	ptr += len;
	loop_len = sdth.section_length - (len - 3 + 4); // 3は共通ヘッダ長 4はCRC
	while(loop_len > 0) {
		len = parseSDTbody(ptr, &sdtb);
		//printf("body %d - %d = %d\n",loop_len,len,loop_len - len);
		ptr += len;
		loop_len -= len;

		desc_len = sdtb.descriptors_loop_length;
		loop_len -= desc_len;
		while(desc_len > 0) {
			if ( *ptr == 0xCF ) {
				len = parseLOGdesc(ptr, &logd);
				ptr += len;
				desc_len -= len;

				#if 0
				printf("LOG=%d,%d,%d,%d\n",
					logd.logo_transmission_type, logd.logo_id, 
					logd.logo_version, logd.download_data_id);
				#endif

				/*
					logd.logo_transmission_type
					0x01 CDT 伝送方式1:CDT をダウンロードデータ識別で直接参照する場合
					0x02 CDT 伝送方式2:CDT をロゴ識別を用いてダウンロードデータ識別を間接的に参照する場合
					0x03 簡易ロゴ方式
				*/
				if ( logd.logo_transmission_type != 0x01 ) continue;
				for (i=0; i<stationi; i++) {
					// FIXME: pStation[i].logo_version < logd.logo_version
					if ( pStation[i].svId == sdtb.service_id ) {
						pStation[i].logo_download_data_id = logd.download_data_id;
						pStation[i].logo_version = logd.logo_version;
					}
				}

				continue;
			}
			else if ( *ptr != 0x48 ) {
				len = parseOTHERdesc(ptr);
				ptr += len;
				desc_len -= len;
				continue;
			}
			len = parseSVCdesc(ptr, &desc);
			//printf("desc %d - %d = %d\n",desc_len,len,desc_len - len);
			ptr += len;
			desc_len -= len;

			rc = serachid(top, sdtb.service_id);
			if(rc == 0){
				svtptr = calloc(1, sizeof(SVT_CONTROL));
				svtptr->original_network_id = sdth.original_network_id;
				svtptr->transport_stream_id = sdth.transport_stream_id;
				svtptr->event_id = sdtb.service_id;
				memcpy(svtptr->servicename, desc.service_name, strlen(desc.service_name));
				if (desc.service_type == 1){
					enqueue_sdt(top, svtptr);

					sprintf(sid, "%s_%d", ontvheader, sdtb.service_id );
					for (i=0; i<stationi; i++) {
						if ( !strcmp( pStation[i].ontv, sid ) ) {
							// 既に同じものが含まれるのでスキップ
							break;
						}
					}

					pStation = realloc(pStation, (stationi + 1) * sizeof(STATION));
					memset(&pStation[stationi], 0, sizeof(STATION));

					pStation[stationi].name = malloc( strlen(desc.service_name) + 1 );
					pStation[stationi].ontv = malloc( strlen(sid) + 1 );
					pStation[stationi].tsId = sdth.transport_stream_id;
					pStation[stationi].onId = sdth.original_network_id;
					pStation[stationi].svId = sdtb.service_id;

					strcpy(pStation[stationi].name, desc.service_name);
					strcpy(pStation[stationi].ontv, sid);

					stationi++;
				}

#if 0
				printf("STATION=%s,%d,%d,%d,%d,%d\n",
					desc.service_name,sdtb.service_id,sdth.transport_stream_id,
					sdth.original_network_id,sdtb.service_id,desc.service_type);

				printf("SDT=%s,%d,%x,%x,%x,%x,%x,%x,%x\n",
					desc.service_name, sdtb.service_id, sdtb.reserved_future_use1,
					sdtb.EIT_user_defined_flags, sdtb.EIT_schedule_flag, sdtb.EIT_present_following_flag,
					sdtb.running_status, sdtb.free_CA_mode, sdtb.descriptors_loop_length);
/*
0x01:デジタルTVサービス
0xA5:プロモーション映像サービス
0x0C:データサービス
*/
				printf("SDT=(%x:%x)%s,%d,%d,%d,%d,%d(%d,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x)\n",
						sdth.table_id, desc.service_type, 
						desc.service_name, sdtb.service_id,
						desc.descriptor_tag, desc.descriptor_length, desc.service_type,
						desc.service_provider_name_length, desc.service_name_length,
						sdth.table_id, sdth.section_syntax_indicator, sdth.reserved_future_use1,
						sdth.reserved1, sdth.section_length, sdth.transport_stream_id,
						sdth.reserved2, sdth.version_number, sdth.current_next_indicator,
						sdth.section_number, sdth.last_section_number, sdth.original_network_id,
						sdth.reserved_future_use2);
#endif
			}
		}
/*
		//ptr += sdtb.descriptors_loop_length;
		loop_len -= sdtb.descriptors_loop_length;
		
		if (loop_len>0){
			ptr += sdtb.descriptors_loop_length;
		}
*/
	}
	*station = pStation;
	*station_count = stationi;
	//printf("stationi %d -",stationi);//stationi==294で落ちる
	return;
}

