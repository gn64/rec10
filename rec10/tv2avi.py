#!/usr/bin/python
# coding: UTF-8
# Rec10 TS Recording Tools
# Copyright (C) 2009-2014 Yukikaze

import datetime
import os
import re
import time
import os.path
import traceback

import chdb
import status
import ts2x264
import tv2ts
import recdblist
import tv2audio
import rec10const

def timetv2b25(pout, chtxt, btime, etime, opt):
    """
    poutはタイトル
    """
    bt = datetime.datetime.strptime(btime, "%Y-%m-%d %H:%M:%S")
    et = datetime.datetime.strptime(etime, "%Y-%m-%d %H:%M:%S")
    extt = os.path.splitext(pout)
    tsout = extt[0]
    tnow = datetime.datetime.now()
    wt = bt-tnow
    waitt = wt.seconds
    if waitt>0:
        time.sleep(waitt)
    tnow = datetime.datetime.now()
    dt = et-tnow
    rectime = dt.seconds-5
    rectime = str(rectime)
    tv2ts.tv2b25ts(tsout + ".ts.b25", chdb.searchCHFromChtxt(chtxt)['ch'], rectime,chdb.searchCHFromChtxt(chtxt)['tsid'])

def b252ts(pout, chtxt, btime, etime, opt):
    """
    poutはタイトル(自動的にtitle.b25 title.tsと名前がつきます。)
    """
    #status.setB25Decoding(status.getB25Decoding() + 1)
    status.changeB25Decoding(1)
    try:
        try:
            chs=chdb.searchCHFromChtxt(chtxt)
            ch=chs['ch']
            csch=chs['csch']
            #print [ch,csch,chtxt]
        except:
            ch="0"
            csch="0"
        tv2ts.b252ts(pout + ".ts", ch, csch)
        tsout = pout
        aviin = pout + ".ts"
        dualaudio = 0
        pentaaudio = 0
        singleaudiosplit = 0
        dualstereo = 0
        if re.search("5", opt):
            pentaaudio = 1
        if re.search("d", opt):
            dualaudio = 1
        if re.search("b", opt):
            singleaudiosplit = 1
        if re.search(u"\[二\]", pout):
            dualaudio = 1
        elif re.search(u'（二）', pout):
            dualaudio = 1
        elif re.search(u'\(二\)', pout):
            dualaudio = 1
        if re.search("s", opt):
            dualstereo = 1
            dualaudio = 0
        if dualaudio == 1:
            tv2audio.ts2dualaudio_BonTsDemux(aviin, rec10const.BONTSDEMUX_DELAY, opt)
        elif pentaaudio == 1:
            tv2audio.ts2pentaaudio_BonTsDemux(aviin, rec10const.BONTSDEMUX_DELAY, opt)
        elif singleaudiosplit == 1:
            tv2audio.ts2single_fp_BonTsDemux(aviin, opt)
        elif dualstereo == 1:
            tv2audio.ts2bistereoaudio_BonTsDemux(aviin, rec10const.BONTSDEMUX_DELAY, opt)
    except Exception, inst:
        recdblist.addCommonlogEX("Error", "b252ts(tv2avi.py)", str(type(inst))+traceback.format_exc(), str(inst))
    status.changeB25Decoding(-1)
    
def ts2avi(pin, pout, opt):
    status.changeEncoding(1)
    try:
        ts2x264.ts2x264(pin, pout, opt)
    except Exception, inst:
        recdblist.printutf8("error occures in tv2avi.py ts2avi")
        recdblist.printutf8(str(type(inst)))
        recdblist.printutf8(str(inst)+traceback.format_exc())
    status.changeEncoding(-1)
    
def ts2raw(pin, pout, opt):
    status.changeEncoding(1)
    try:
        ts2x264.ts2x264(pin, pout, opt)
    except Exception, inst:
        recdblist.printutf8("error occures in tv2avi.py ts2raw")
        recdblist.printutf8(str(type(inst)))
        recdblist.printutf8(str(inst)+traceback.format_exc())
    status.changeEncoding(-1)
    
def ts2m4v(pin,pout,opt):
    status.changeEncoding(1)
    try:
        ts2x264.ts2x264(pin, pout, opt)
    except Exception, inst:
        recdblist.printutf8("error occures in tv2avi.py ts2m4v")
        recdblist.printutf8(str(type(inst)))
        recdblist.printutf8(str(inst)+traceback.format_exc())
    status.changeEncoding(-1)

