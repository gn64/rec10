#!/usr/bin/python
# coding: UTF-8
# Rec10 TS Recording Tools
# Copyright (C) 2009-2014 Yukikaze
import os
import os.path
import time
import optparse
import sys
import datetime

import configreader
import chdb
import tv2mp4
import tv2ts
def testAllCHEncode(option,stime):
    chl=chdb.getAllCH()
    print chl
    path= configreader.getConfPath("test")
    tpath=os.path.join(path, "encode_test")
    if not os.path.exists(tpath):
        os.mkdir(tpath)
    dnowt=datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")+"_"+option+""
    tpatht=os.path.join(tpath,dnowt)
    if not os.path.exists(tpatht):
        os.mkdir(tpatht)
    if len(chl)>0:
        for t in chl:
            print t
            pathname=os.path.join(tpatht,t['chtxt'])
            print pathname
            tv2ts.tv2ts(pathname+".ts",t['ch'],t['csch'], stime,t['tsid'])
            tv2mp4.ts2mp4(pathname+".ts", pathname+".mp4", option)
            time.sleep(2)
if __name__ == "__main__":
    usage="usage: "
    version="%prog 0.9.10"
    parser=optparse.OptionParser(usage=usage,version=version)
    parser.add_option("-e","--Encode",action="store",type="string",dest="encode_option",default="",metavar="TITLE",help="encode test(option)")
    (opts,args)=parser.parse_args(sys.argv)
    if opts.encode_option != "":##-sの場合
        testAllCHEncode(opts.encode_option, "10")
