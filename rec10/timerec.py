#!/usr/bin/python
# coding: UTF-8
# Rec10 TS Recording Tools
# Copyright (C) 2009-2014 Yukikaze
import datetime
import os
import os.path
import re
import sys
import time
import shutil
import traceback

import auto_rec
import auto_process
import chdb
import configreader
import epgdb
import rec10d
import recdb
import status
import tv2audio
import tv2avi
import tv2mkv
import tv2mp4
import install
import recdblist
import recque
import rec10const
recpath = configreader.getConfPath('recpath')
movepath = configreader.getConfPath('move_destpath')
path = str(os.path.dirname(os.path.abspath(__file__))) + "/"
def task():
    """
    数分毎に実行されるタスク処理
    予定によって子プロセスを生成し処理する。
    """
    try:
        if rec10d.rec10db.select_installed_in_status()==0 or rec10d.rec10db.select_version_in_status()<rec10const.version:
            install.install()
        elif rec10d.rec10db.select_installed_in_status()==1:
            import scan_ch
            rec10d.rec10db.new_epg_ch()
            scan_ch.searchCh()
            rec10d.rec10db.change_installed_in_status(100)
        elif rec10d.rec10db.select_installed_in_status()==2:
            recdblist.printutf8ex("Exit because CH scanning.", 200, 200)
            sys.exit(0)
    except Exception, inst:
        recdblist.addCommonlogEX("Error", "install_check(timerec.py)", str(type(inst)),str(inst)+traceback.format_exc(),log_level=200)
        #install.install()
    recdb.deleteOldProgramBeforeTheseHours("24")
    recdb.delete_old_auto_bayes("1")
    recdb.delete_old_auto_keyword("1")
    tasks = recdb.getProgramsInTheseHours("3")
    inum = recdb.countRecNow_minutes("10")
    recdblist.printutf8(u"rec10処理開始"+ datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),verbose_level=800)
    recdblist.printutf8(u"直近録画予約件数:" + str(inum) + u"BS/CS録画中件数:" + str(status.getBSCSRecording()) + u"TE録画中件数:" + str(status.getTERecording()) ,verbose_level=800)
    encodenum=0
    b25num=0
    dnow=datetime.datetime.now()
    if dnow.minute % 10 < 5:
        pid = os.fork()
        if pid != 0:
            ""
        else:
            search_keyword(recdb.getAllJbkKeyword())
            recque.searchRecQue(recpath)
            sys.exit()
    else:
        pid = os.fork()
        if pid!=0:
            ""
        else:
            time.sleep(10)
            auto_process.autoCheck(recpath)
            time.sleep(10)
            auto_process.killDeadEncode(recpath)
            sys.exit()
    update = chdb.update()
    updatelogo = chdb.updateLogo()
    updatelogo=[]
    if len(update) > 0:
        pid = os.fork()
        if pid != 0:
            ""
        else:
            i = 0
            for bctype in update:
                recnum = 0
                if bctype.find('te') > -1:
                    recnum = status.getTERecording() + recdb.countRecNow_minutes_TE("10")
                    recdblist.printutf8(u"放送種別:"+bctype + u"||該当チューナー実行中件数:" + str(status.getTERecording()) + u":直近予約件数:" + str(recdb.countRecNow_minutes_TE("10")),verbose_level=800)
                    recnum = int(configreader.getConfEnv("te_max")) -recnum
                else:
                    recnum = status.getBSCSRecording() + recdb.countRecNow_minutes_BSCS("10")
                    recdblist.printutf8(u"放送種別:"+bctype + u"||該当チューナー実行中件数:" + str(status.getBSCSRecording()) + u":直近予約件数:" + str(recdb.countRecNow_minutes_BSCS("10")),verbose_level=800)
                    recnum = int(configreader.getConfEnv("bscs_max")) -recnum
                if recnum >0 :
                    recdblist.printutf8(str(update),verbose_level=750)
                    rec10d.rec10db.update_status_by_bctype_epg_ch(bctype, "0")
                    epgdb.updatebc(bctype)
                    update = chdb.update()
                    i = i + 1
                if i > 0:
                    break
            time.sleep(5)
            auto_process.update_all_timeline_epg()
            sys.exit()
    elif len(updatelogo)>0:
        pid = os.fork()
        if pid != 0:
            ""
        else:
            i = 0
            for bctype in updatelogo:
                recnum = 0
                if bctype.find('te') > -1:
                    recnum = status.getTERecording() + recdb.countRecNow_minutes_TE("1200")
                    recdblist.printutf8(u"ロゴ未取得 : 放送種別-"+bctype + u"||該当チューナー実行中件数:" + str(status.getTERecording()) + u":直近予約件数:" + str(recdb.countRecNow_minutes_TE("10")),verbose_level=800)
                    recnum = int(configreader.getConfEnv("te_max")) -recnum
                else:
                    recnum = status.getBSCSRecording() + recdb.countRecNow_minutes_BSCS("1200")
                    recdblist.printutf8(u"ロゴ未取得 : 放送種別-"+bctype + u"||該当チューナー実行中件数:" + str(status.getBSCSRecording()) + u":直近予約件数:" + str(recdb.countRecNow_minutes_BSCS("10")),verbose_level=800)
                    recnum = int(configreader.getConfEnv("bscs_max")) -recnum
                if recnum >0 :
                    recdblist.printutf8(u"ロゴ取得"+str(updatelogo),verbose_level=750)
                    rec10d.rec10db.update_logostatus_by_bctype_epg_ch(bctype,"0")
                    epgdb.updateLogo_bc(bctype)
                    updatelogo = chdb.updateLogo()
                    i = i + 1
                if i > 0:
                    break
            time.sleep(5)
            sys.exit()
    else:
        pid = os.fork()
        if pid != 0:
            ""
        else:
            auto_process.update_all_timeline_epg()
            sys.exit()
    for task in tasks:
        typetxt = task["type"]
        try:
            chtxt = task['chtxt']
        except:
            chtxt = ""
        try:
            title = task['title']
        except:
            title = ""
        try:
            btime = task['btime']
            bt = datetime.datetime.strptime(btime, "%Y-%m-%d %H:%M:%S")
        except:
            btime = ""
            bt = datetime.datetime.strptime("2009-01-01 00:00:00", "%Y-%m-%d %H:%M:%S")
        try:
            etime = task['etime']
            et = datetime.datetime.strptime(etime, "%Y-%m-%d %H:%M:%S")
        except:
            etime = ""
            et = datetime.datetime.strptime("2009-01-01 00:00:00", "%Y-%m-%d %H:%M:%S")
        try:
            opt = task['opt']
        except:
            opt = ""
        tnow = datetime.datetime.now()
        dtt = bt-tnow
        dt = dtt.days * 24 * 60 * 60 + dtt.seconds
        if task["type"] == rec10const.REC_RESERVE:#"res,"+chtxt+","+title+","+btime+","+etime+","+opt
            pid = os.fork()
            if pid != 0:#親プロセスの場合
                ""
            else:#子プロセスの場合　アップデートを行って終了
                type_reserve(task["type"],chtxt,title,bt,et,opt)
                sys.exit()
        elif task["type"] == rec10const.REC_KEYWORD:#"key,"+chtxt+","+keyword+","+btime+","+deltatime+","+opt
            deltatime = task['deltatime']
            pid = os.fork()
            if pid != 0:#親プロセスの場合
                ""
            else:#子プロセスの場合　アップデートを行って終了
                type_keyword(task["type"],chtxt,title, bt, et, opt, deltatime)
                sys.exit()
        elif task["type"] == rec10const.REC_KEYWORD_EVERY_SOME_DAYS:#"keyevery,"+chtxt+","+keyword+","+btime+","+deltatime+","+opt+","+deltaday
            deltatime = task['deltatime']
            deltaday = task['deltaday']
            try:
                keyeverycounter=task['counter']
            except:
                keyeverycounter=-1
            pid = os.fork()
            if pid != 0:#親プロセスの場合
                ""
            else:#子プロセスの場合　アップデートを行って終了
                type_keyword_every_day(task["type"],chtxt, title, bt, et, opt, deltatime, deltaday,keyeverycounter)
                sys.exit()
        elif task["type"] == rec10const.REC_FINAL_RESERVE:#"rec,"+chtxt+","+title+","+btime+","+etime+","+opt
            pid = os.fork()
            if pid != 0:#親プロセスの場合
                ""
            else:#子プロセスの場合　アップデートを行って終了
                type_final(task["type"],chtxt, title, bt, et, opt)
                sys.exit()
        elif task["type"] == rec10const.REC_TS_DECODE_QUE:
            b25num=b25num+1
            pid = os.fork()
            if pid != 0:#親プロセスの場合
                ""
            else:#子プロセスの場合　アップデートを行って終了
                time.sleep(5*b25num)
                type_decode_que(task["type"],chtxt, title, bt, et, opt)
                sys.exit()
        elif task["type"] == rec10const.REC_ENCODE_QUE:
            encodenum=encodenum+1
            pid = os.fork()
            if pid != 0:#親プロセスの場合
                ""
            else:#子プロセスの場合　アップデートを行って終了
                time.sleep(5*encodenum)
                type_encode_que(task["type"],chtxt, title, bt, et, opt)
                sys.exit()
        elif task["type"] == rec10const.REC_AVI_TO_MKV:
            if dt < 10 * 60:
                pid = os.fork()
                if pid > 0:#親プロセスの場合
                    ""
                else:
                    makeMP4=1
                    try:
                        if configreader.getConfEnv("make_mp4")==0:
                            makeMP4=0
                    except:
                        ""
                    if re.search("m", opt):
                        makeMP4=0
                    if re.search("4", opt):
                        makeMP4=1
                    recdb.deleteReckey(rec10const.REC_AVI_TO_MKV, title, chtxt, btime)
                    recdb.reserveReckey(rec10const.REC_CHANGING_CANTAINER, title, chtxt, btime, etime, opt)
                    if makeMP4==1:
                        tv2mp4.raw2mp4(os.path.join(recpath,title+".264"),os.path.join(recpath,title+".mp4"),opt)
                    else:
                        tv2mp4.raw2mp4(os.path.join(recpath,title+".264"),os.path.join(recpath,title+".mp4"),opt)
                        tv2mkv.mp42mkv(os.path.join(recpath,title+".mp4"), os.path.join(recpath,title+".mkv"))
                        os.remove(os.path.join(recpath,title+".mp4"))
                    recdb.deleteReckey(rec10const.REC_CHANGING_CANTAINER, title, chtxt, btime)
                    sys.exit()
        elif task["type"] == rec10const.REC_AVI_TO_MP4:
            if dt < 10 * 60:
                pid = os.fork()
                if pid > 0:#親プロセスの場合
                    ""
                else:
                    makeMP4=0
                    try:
                        if configreader.getConfEnv("make_mp4")==1:
                            makeMP4=1
                    except:
                        ""
                    if re.search("m", opt):
                        makeMP4=0
                    if re.search("4", opt):
                        makeMP4=1
                    recdb.deleteReckey(rec10const.REC_AVI_TO_MP4, title, chtxt, btime)
                    recdb.reserveReckey(rec10const.REC_CHANGING_CANTAINER, title, chtxt, btime, etime, opt)
                    if makeMP4==1:
                        tv2mp4.ts2mp4(os.path.join(recpath,title+".ts"), os.path.join(recpath,title+".mp4"), opt)
                        tv2mp4.raw2mp4(os.path.join(recpath,title+".264"),os.path.join(recpath,title+".mp4"),opt)
                    else:
                        tv2mkv.raw2mkv(os.path.join(recpath,title+".264"),os.path.join(recpath,title+".mkv"),opt)
                    recdb.deleteReckey(rec10const.REC_CHANGING_CANTAINER, title, chtxt, btime)
                    sys.exit()
        elif task["type"] == rec10const.REC_MKV_TO_MP4:
            if dt < 10 * 60:
                pid = os.fork()
                if pid > 0:#親プロセスの場合
                    ""
                else:
                    recdb.deleteReckey(rec10const.REC_MKV_TO_MP4, title, chtxt, btime)
                    recdb.reserveReckey(rec10const.REC_CHANGING_CANTAINER, title, chtxt, btime, etime, opt)
                    tv2mp4.mkv2mp4(os.path.join(recpath,title+".mkv"),os.path.join(recpath,title+".mp4"))
                    recdb.deleteReckey(rec10const.REC_CHANGING_CANTAINER, title, chtxt, btime)
                    sys.exit()
        elif task["type"] == rec10const.REC_AUDIO_VIDEO_MUX:
            if dt < 10 * 60:
                pid = os.fork()
                if pid > 0:#親プロセスの場合
                    ""
                else:
                    recdb.deleteReckey(rec10const.REC_AUDIO_VIDEO_MUX, title, chtxt, btime)
                    recdb.reserveReckey(rec10const.REC_AUDIO_VIDEO_MUXING, title, chtxt, btime, etime, opt)
                    tv2mp4.ts2mp4(os.path.join(recpath,title+".ts"),os.path.join(recpath,title+".mp4"),opt,skipVideoEncode=1)
                    recdb.deleteReckey(rec10const.REC_AUDIO_VIDEO_MUXING, title, chtxt, btime)
                    recdb.reserveReckey(rec10const.REC_FIN_LOCAL, title, chtxt, btime, etime, opt)
                    sys.exit()
    sys.exit()
def search_keyword(key):
    tnow = datetime.datetime.now()
    nows =tnow.strftime("%Y-%m-%d %H:%M:%S")
    for k,auto,opt in key:
        recdatum = epgdb.searchTimeAuto(k,nows, "144")
        for recdata in recdatum:
            if recdata[1] != "":
                chtxtt = recdata[0]
                titlet = recdata[1]
                btimet = recdata[2]
                etimet = recdata[3]
                btt = datetime.datetime.strptime(btimet, "%Y-%m-%d %H:%M:%S")
                ett = datetime.datetime.strptime(etimet, "%Y-%m-%d %H:%M:%S")
                btimet = btt.strftime("%Y-%m-%d %H:%M:%S")
                etimet = ett.strftime("%Y-%m-%d %H:%M:%S")
                if auto==1 or status.getSettings_auto_jbk()==1:
                    topt=opt
                    if topt==None:
                        topt=status.getSettings_auto_opt()
                    if len(topt)==0:
                        topt=status.getSettings_auto_opt()
                    try:
                        maxnum=0
                        ch=chdb.searchCHFromChtxt(chtxtt)
                        if len(ch['ch'])>2:
                            maxnum=epgdb.countSchedule(btimet, etimet)[1]
                            maxnum=int(configreader.getConfEnv("bscs_max"))-maxnum
                        else:
                            maxnum=epgdb.countSchedule(btimet, etimet)[0]
                            maxnum=int(configreader.getConfEnv("te_max"))-maxnum
                        if maxnum>0:
                            if recdb.checkDuplicated(titlet, chtxtt, btimet, etimet)==0:
                                recdb.reserveReckey(rec10const.REC_RESERVE,titlet,chtxtt, btimet, etimet,topt)
                    except Exception, inst:
                        recdblist.addCommonlogEX("Error", "search_keyword_auto_jbk(timerec.py)", str(type(inst)),str(inst)+traceback.format_exc(),log_level=200)
                try:
                    recdb.reserveAutoKeyword(chtxtt, titlet, btimet, etimet)
                except Exception, inst:
                    recdblist.addCommonlogEX("Error", "search_keyword(timerec.py)", str(type(inst)),str(inst)+traceback.format_exc(),log_level=200)
def type_reserve(typetxt,chtxt,title,bt,et,opt):
    btime = bt.strftime("%Y-%m-%d %H:%M:%S")
    etime = et.strftime("%Y-%m-%d %H:%M:%S")
    typetxtnow=typetxt
    typetxtfinal=""
    if typetxt==rec10const.REC_RESERVE:
        typetxtfinal=rec10const.REC_FINAL_RESERVE
    tnow = datetime.datetime.now()
    dtt = bt-tnow
    dt = dtt.days * 24 * 60 * 60 + dtt.seconds
    if (dt < 58 * 60 and dt > 30 * 60):
        bctypet = chdb.searchCHFromChtxt(chtxt)['bctype']
        chdatat = rec10d.rec10db.select_by_bctype_epg_ch(bctypet)
        dt1 = bt - datetime.datetime.strptime(chdatat[0][5], "%Y-%m-%d %H:%M:%S")
        dt1 = dt1.days * 24 * 60 * 60 + dt1.seconds
        if dt1 < 60 * 60:
            recdata = epgdb.searchTime(title, btime, "5", chtxt)
            chtxtn = recdata[0]
            titlen = recdata[1]
            btimen = recdata[2]
            etimen = recdata[3]
            exp = recdata[4]
            longexp = recdata[5]
            category=recdata[6]
            bt = datetime.datetime.strptime(btimen, "%Y-%m-%d %H:%M:%S")
            et = datetime.datetime.strptime(etimen, "%Y-%m-%d %H:%M:%S")
            btimen = bt.strftime("%Y-%m-%d %H:%M:%S")
            etimen = et.strftime("%Y-%m-%d %H:%M:%S")
            if chtxt != "":
                try:
                    recdb.deleteReckey(typetxtnow, title, chtxt, btime)
                    recdb.reserveReckey(typetxtfinal, titlen, chtxtn, btimen, etimen, opt)
                    recdb.addRecLogProgram(titlen, chtxtn, btimen, etimen, opt, exp, longexp, category)
                    auto_rec.addKey(chtxt, titlen,exp+" "+longexp)
                    auto_rec.addKey("ALL", titlen,exp+" "+longexp)
                    recdblist.printutf8(u"追いかけ機能実行中: "+title+" : "+titlen+" "+btimen+" "+etimen)
                except Exception, inst:
                    recdblist.addCommonlogEX("Error", "Oikake DB(timerec.py)", str(type(inst)),str(inst)+traceback.format_exc(),log_level=200)
            else:
                recdblist.printutf8(u"追いかけ機能エラー：番組データが見付かりません。")
        else:
            if rec10d.rec10db.select_by_bctype_epg_ch(bctypet)[0][6] != "0":
                rec10d.rec10db.update_status_by_bctype_epg_ch(bctypet, "3")
        sys.exit()
    elif (dt <= 30 * 60 and dt > 20 * 60):
        recdata = epgdb.searchTime(title, btime, "5", chtxt)
        chtxtn = recdata[0]
        titlen = recdata[1]
        btimen = recdata[2]
        etimen = recdata[3]
        exp = recdata[4]
        longexp = recdata[5]
        category=recdata[6]
        bt = datetime.datetime.strptime(btimen, "%Y-%m-%d %H:%M:%S")
        et = datetime.datetime.strptime(etimen, "%Y-%m-%d %H:%M:%S")
        btimen = bt.strftime("%Y-%m-%d %H:%M:%S")
        etimen = et.strftime("%Y-%m-%d %H:%M:%S")
        if chtxt != "":
            try:
                recdb.deleteReckey(typetxtnow, title, chtxt, btime)
                recdb.reserveReckey(typetxtfinal, titlen, chtxtn, btimen, etimen, opt)
                recdb.addRecLogProgram(titlen, chtxtn, btimen, etimen, opt, exp, longexp, category)
                auto_rec.addKey(chtxt, titlen,exp+" "+longexp)
                auto_rec.addKey("ALL", titlen,exp+" "+longexp)
                recdblist.addCommonlogEX(u"通常", "Oikake (timerec.py)",u"追いかけ機能実行中",u"追いかけ機能実行中: "+title+" : "+titlen+" "+btimen+" "+etimen,log_level=500)
            except Exception, inst:
                recdblist.addCommonlogEX("Error", "Oikake DB(timerec.py)", str(type(inst)),str(inst)+traceback.format_exc(),log_level=200)
    elif dt <= 20 * 60:
        try:
            recdb.deleteReckey(typetxtnow, title, chtxt, btime)
            recdb.reserveReckey(typetxtfinal, title, chtxt, btime, etime, opt)
        except Exception, inst:
            recdblist.addCommonlogEX("Error", "Oikake DB(timerec.py)", str(type(inst)),str(inst)+traceback.format_exc(),log_level=200)
def type_final(typetxt,chtxt,title,bt,et,opt):
    btime = bt.strftime("%Y-%m-%d %H:%M:%S")
    etime = et.strftime("%Y-%m-%d %H:%M:%S")
    typetxtnow=typetxt
    typetxting=""
    typetxtdecque=""
    if typetxt==rec10const.REC_FINAL_RESERVE:
        typetxting=rec10const.REC_TS_RECORDING
        typetxtdecque=rec10const.REC_TS_DECODE_QUE
    tnow = datetime.datetime.now()
    dtt = bt-tnow
    dt = dtt.days * 24 * 60 * 60 + dtt.seconds
    if dt < 6 * 60 and dt > 0:
        newtitle=title
        recdb.deleteReckey(typetxtnow, title, chtxt, btime)
        testpath=[os.path.join(recpath,title+".ts.b25")]
        testpath.append(os.path.join(recpath,title+".ts"))
        testpath.append(os.path.join(recpath,title+".avi"))
        testpath.append(os.path.join(recpath,title+".mp4"))
        testpath.append(os.path.join(recpath,title+".log"))
        tcheck=0
        for ti in testpath:
            if os.path.exists(ti):
                tcheck=tcheck+1
        if re.search("N", opt) or tcheck>0:
            iff=""
            try:
                iff=u"("+configreader.getConfEnv("iff")+u")_"
            except:
                iff=""
            newtime=bt
            newtitle=newtitle+u"_"+iff+newtime.strftime("%Y-%m-%dT%H-%M-%S")
        recdb.reserveReckey(typetxting, newtitle, chtxt, btime, etime, opt)
        recdblist.addCommonlogEX(u"通常","timerec.py",u"録画開始 "+newtitle+" "+btime+" "+etime,"",log_level=500)
        tv2avi.timetv2b25(recpath + "/" + newtitle + ".avi", chtxt, btime, etime, opt)
        recdb.deleteReckey(typetxting, newtitle, chtxt, btime)
        if not re.search("R", opt):
            tnow = datetime.datetime.now()
            bt = datetime.datetime.strptime(btime, "%Y-%m-%d %H:%M:%S")
            et = datetime.datetime.strptime(etime, "%Y-%m-%d %H:%M:%S")
            dt = tnow-bt
            bt = tnow + datetime.timedelta(seconds=600)
            et = et + dt + datetime.timedelta(seconds=600)
            btime = bt.strftime("%Y-%m-%d %H:%M:%S")
            etime = et.strftime("%Y-%m-%d %H:%M:%S")
            recdb.reserveReckey(typetxtdecque, newtitle, chtxt, btime, etime, opt)
        else:
            try:
                try:
                    shutil.copy(os.path.join(recpath,title+".ts.b25"), os.path.join(movepath,title+".ts.b25"))
                except:
                    ""
                try:
                    os.chmod(os.path.join(movepath,title+".ts.b25"),0777)
                except:
                    ""
                recque.writeRecQue(movepath, chtxt, title, opt)
                try:
                    os.chmod(os.path.join(movepath,title+".recq"),0777)
                except:
                    ""
            except Exception, inst:
                recdblist.addCommonlogEX("Error", "Move option(timerec.py)", str(type(inst)),str(inst)+traceback.format_exc(),log_level=200)
            recdb.reserveReckey(rec10const.REC_MOVE_END, newtitle, chtxt, btime, etime, opt)
        sys.exit()
def type_keyword(typetxt,chtxt,title,bt,et,opt,deltatime):
    btime = bt.strftime("%Y-%m-%d %H:%M:%S")
    etime = et.strftime("%Y-%m-%d %H:%M:%S")
    typetxtnow=typetxt
    typetxtres=""
    if typetxt==rec10const.REC_KEYWORD:
        typetxtres=rec10const.REC_RESERVE
    tnow = datetime.datetime.now()
    dtt = bt-tnow
    dt = dtt.days * 24 * 60 * 60 + dtt.seconds
    recdblist.printutf8(str(dt), verbose_level=800)
    if dt <= 90 * 60 and dt > 70 * 60:
        recdata = epgdb.searchTime(title, btime, deltatime, chtxt)
        if recdata[1] != "":
            chtxtt = recdata[0]
            titlet = recdata[1]
            btimet = recdata[2]
            etimet = recdata[3]
            exp = recdata[4]
            longexp = recdata[5]
            category=recdata[6]
            bt = datetime.datetime.strptime(btimet, "%Y-%m-%d %H:%M:%S")
            et = datetime.datetime.strptime(etimet, "%Y-%m-%d %H:%M:%S")
            btimet = bt.strftime("%Y-%m-%d %H:%M:%S")
            etimet = et.strftime("%Y-%m-%d %H:%M:%S")
            #try:
            recdb.deleteReckey(typetxtnow, title, chtxt, btime)
            recdb.reserveReckey(typetxtres, titlet, chtxt, btimet, etimet, opt)
            recdb.addRecLogProgram(titlet, chtxtt, btimet, etimet, opt, exp,longexp,category)
            auto_rec.addKey(chtxt, titlet,exp+" "+longexp)
            auto_rec.addKey("ALL", titlet,exp+" "+longexp)
            recdblist.addCommonlogEX(u"通常","timerec.py",u"key "+title+u" : "+titlet+u" "+btimet+u" "+etimet,"",log_level=500)
            #except Exception, inst:
            #    recdblist.printutf8("Error happened in REC_KEYWORD DB")
            #    recdblist.printutf8(type(inst))
            #    recdblist.printutf8(inst)
        else:
            recdblist.printutf8("nothing match")
    elif dt <= 70 * 60:
        recdb.deleteReckey(typetxtnow, title, chtxt, btime)
        recdb.reserveReckey(typetxtres, title, chtxt, btime, etime, opt)
        recdblist.addCommonlogEX(u"エラー","timerec.py",u"nothing match","",log_level=200)
        recdblist.addCommonlogEX(u"エラー","timerec.py",u"key "+title+u" "+btime+u" "+etime,"",log_level=200)
def type_keyword_every_day(type,chtxt,title,bt,et,opt,deltatime,deltaday,counter):
    btime = bt.strftime("%Y-%m-%d %H:%M:%S")
    etime = et.strftime("%Y-%m-%d %H:%M:%S")
    tnow = datetime.datetime.now()
    dtt = bt-tnow
    dt = dtt.days * 24 * 60 * 60 + dtt.seconds
    dd = datetime.timedelta(days=int(deltaday))
    if dtt.days < 0:
        recdb.deleteReckey(rec10const.REC_KEYWORD_EVERY_SOME_DAYS, title, chtxt, btime)
        bt = bt + dd
        et = et + dd
        btxt = bt.strftime("%Y-%m-%d %H:%M:%S")
        etxt = et.strftime("%Y-%m-%d %H:%M:%S")
        if counter>0:
            recdb.reserveEverydayKeyword(title, chtxt, btxt, etxt, deltatime, opt, deltaday,counter-1)
        elif counter==-1:
            recdb.reserveEverydayKeyword(title, chtxt, btxt, etxt, deltatime, opt, deltaday,-1)
    elif dt < 120 * 60:
        recdb.deleteReckey(rec10const.REC_KEYWORD_EVERY_SOME_DAYS, title, chtxt, btime)
        bt = bt + dd
        et = et + dd
        btxt = bt.strftime("%Y-%m-%d %H:%M:%S")
        etxt = et.strftime("%Y-%m-%d %H:%M:%S")
        if counter>0:
            recdb.reserveEverydayKeyword(title, chtxt, btxt, etxt, deltatime, opt, deltaday,counter-1)
        elif counter==-1:
            recdb.reserveEverydayKeyword(title, chtxt, btxt, etxt, deltatime, opt, deltaday,-1)
        recdata = epgdb.searchTime(title, btime, deltatime, chtxt)
        if recdata[1] != "":
            chtxtt = recdata[0]
            titlet = recdata[1]
            btimet = recdata[2]
            etimet = recdata[3]
            exp = recdata[4]
            longexp = recdata[5]
            category=recdata[6]
            bt = datetime.datetime.strptime(btimet, "%Y-%m-%d %H:%M:%S")
            et = datetime.datetime.strptime(etimet, "%Y-%m-%d %H:%M:%S")
            btimet = bt.strftime("%Y-%m-%d %H:%M:%S")
            etimet = et.strftime("%Y-%m-%d %H:%M:%S")
            #try:
            recdb.reserveKeyword(titlet, chtxt, btimet, etimet, deltatime, opt)
        else:
            recdb.reserveKeyword(title, chtxt, btime, etime, deltatime, opt)
def type_decode_que(typetxt,chtxt,title,bt,et,opt):
    btime = bt.strftime("%Y-%m-%d %H:%M:%S")
    etime = et.strftime("%Y-%m-%d %H:%M:%S")
    tnow = datetime.datetime.now()
    dtt = bt-tnow
    dt = dtt.days * 24 * 60 * 60 + dtt.seconds
    typetxtnow=typetxt
    typetxting=""
    typetxtmiss=""
    typetxtencque=""
    if typetxt== rec10const.REC_TS_DECODE_QUE:
        typetxting=rec10const.REC_TS_DECODING
        typetxtmiss=rec10const.REC_MISS_DECODE
        typetxtencque=rec10const.REC_ENCODE_QUE
    tnow = datetime.datetime.now()
    dtt = bt-tnow
    dt = dtt.days * 24 * 60 * 60 + dtt.seconds
    if dt < 10 * 60:
        if status.getB25Decoding() < 2:
            pin = recpath + "/" + title
            recdb.deleteReckey(typetxtnow, title, chtxt, btime)
            recdb.reserveReckey(typetxting, title, chtxt, btime, etime, opt)
            tv2avi.b252ts(pin, chtxt, btime, etime, opt)
            recdb.deleteReckey(typetxting, title, chtxt, btime)
            if not os.access(recpath + "/" + title + ".ts", os.F_OK):
                recdb.deleteReckey(typetxting, title, chtxt, btime)
                recdb.reserveReckey(typetxtmiss, title, chtxt, btime, etime, opt)
            else:
                auto_process.deleteTmpFile(recpath, title, ".ts")
            bt = datetime.datetime.strptime(btime, "%Y-%m-%d %H:%M:%S")
            et = datetime.datetime.strptime(etime, "%Y-%m-%d %H:%M:%S")
            dt = et-bt
            if not re.search("D", opt):
                tnow = datetime.datetime.now()
                bt = tnow + datetime.timedelta(seconds=600)
                et = bt + dt
                btime = bt.strftime("%Y-%m-%d %H:%M:%S")
                etime = et.strftime("%Y-%m-%d %H:%M:%S")
                recdb.reserveReckey(typetxtencque, title, chtxt, btime, etime, opt)
            else:
                try:
                    try:
                        shutil.copy(os.path.join(recpath,title+".ts"), os.path.join(movepath,title+".ts"))
                    except:
                        ""
                    try:
                        os.chmod(os.path.join(movepath,title+".ts"),0777)
                    except:
                        ""
                    recque.writeRecQue(movepath, chtxt, title, opt)
                    try:
                        os.chmod(os.path.join(movepath,title+".recq"),0777)
                    except:
                        ""
                    time.sleep(5)
                    if os.path.getsize(os.path.join(recpath,title+".ts"))==os.path.getsize(os.path.join(movepath,title+".ts")):
                        os.remove(os.path.join(recpath,title+".ts"))
                except Exception, inst:
                    errtxt1="move ts error.\n"
                    errtxt2=str(type(inst))+"\n"
                    errtxt2=errtxt2+str(inst)
                    recdblist.addCommonlogEX("Error", "type_decode_que(timerec.py)", errtxt1,errtxt2+traceback.format_exc(),log_level=200)
                recdb.reserveReckey(rec10const.REC_MOVE_END, title, chtxt, btime, etime, opt)
        else:
            recdb.deleteReckey(typetxtnow, title, chtxt, btime)
            bt = bt + datetime.timedelta(seconds=600)
            et = et + datetime.timedelta(seconds=600)
            btime = bt.strftime("%Y-%m-%d %H:%M:%S")
            etime = et.strftime("%Y-%m-%d %H:%M:%S")
            recdb.reserveReckey(typetxtnow, title, chtxt, btime, etime, opt)
    sys.exit()
def type_encode_que(typetxt,chtxt,title,bt,et,opt):
    btime = bt.strftime("%Y-%m-%d %H:%M:%S")
    etime = et.strftime("%Y-%m-%d %H:%M:%S")
    tnow = datetime.datetime.now()
    dtt = bt-tnow
    dt = dtt.days * 24 * 60 * 60 + dtt.seconds
    typetxtnow=typetxt
    typetxting=""
    typetxtfin=""
    if typetxt==rec10const.REC_ENCODE_QUE:
        typetxting=rec10const.REC_ENCODE_LOCAL
        typetxtfin=rec10const.REC_FIN_LOCAL
    if dt < 10 * 60:
        if status.getEncoding() < int(configreader.getConfEnv("enc_max")):
            recdb.deleteReckey(typetxtnow, title, chtxt, btime)
            recdb.reserveReckey(typetxting, title, chtxt, btime, etime, opt)
            recdblist.printutf8(opt)
            pin = recpath + "/" + title + ".ts"
            if re.search("d", opt):
                pin = recpath + "/" + title + ".m2v"
                if not os.path.exists(pin) or os.path.getsize(pin)<100*1000:
                    paac2 = recpath + "/" + title + "_2.aac"
                    pmp32 = recpath + "/" + title + "_2.mp3"
                    if not os.path.exists(paac2) and not os.path.exists(pmp32):
                        tv2audio.ts2dualaudio_BonTsDemux(os.path.join(recpath, title+".ts"),rec10const.BONTSDEMUX_DELAY,opt)
                time.sleep(3)
            elif re.search("5", opt):
                pin = recpath + "/" + title + ".m2v"
                if not os.path.exists(pin) or os.path.getsize(pin)<100*1000:
                    paac2 = recpath + "/" + title + "_2.aac"
                    pmp32 = recpath + "/" + title + "_2.mp3"
                    if not os.path.exists(paac2) and not os.path.exists(pmp32):
                        tv2audio.ts2pentaaudio_BonTsDemux(os.path.join(recpath, title+".ts"), rec10const.BONTSDEMUX_DELAY, opt)
                time.sleep(3)
            elif re.search("b", opt):
                pin = recpath + "/" + title + ".m2v"
                if not os.path.exists(pin) or os.path.getsize(pin)<100*1000:
                    paac2 = recpath + "/" + title + ".aac"
                    pmp32 = recpath + "/" + title + ".mp3"
                    if not os.path.exists(paac2) and not os.path.exists(pmp32):
                        tv2audio.ts2single_fp_BonTsDemux(os.path.join(recpath, title+".ts"),opt)
                time.sleep(3)
            elif re.search("s", opt):
                pin = recpath + "/" + title + ".m2v"
                paac2 = recpath + "/" + title + "_2.aac"
                pmp32 = recpath + "/" + title + "_2.mp3"
                if not os.path.exists(paac2) and not os.path.exists(pmp32):
                    tv2audio.ts2bistereoaudio_BonTsDemux(os.path.join(recpath, title+".ts") , rec10const.BONTSDEMUX_DELAY ,opt)
                time.sleep(3)
            makeMP4=0
            try:
                if configreader.getConfEnv("make_mp4")=="1":
                    makeMP4=1
            except:
                ""
            if re.search("m", opt):
                makeMP4=0
            if re.search("4", opt):
                makeMP4=1
            recdblist.printutf8(pin)
            if makeMP4==1:
                pout = recpath + "/" + title + ".mp4"
                tv2mp4.ts2mp4(pin, pout, opt)
            else:
                pout = recpath + "/" + title + ".mkv"
                tv2mkv.ts2mkv(pin, pout, opt)
            recdb.deleteReckey(typetxting, title, chtxt, btime)
            time.sleep(10)
            if re.search("E", opt):
                try:
                    if os.path.exists(os.path.join(recpath,title+".mp4")):
                        try:
                            shutil.copy(os.path.join(recpath,title+".mp4"), os.path.join(movepath,title+".mp4"))
                        except:
                            ""
                        try:
                            os.chmod(os.path.join(movepath,title+".mp4"),0777)
                        except:
                            ""
                        recque.writeRecQue(movepath, chtxt, title, opt)
                        try:
                            os.chmod(os.path.join(movepath,title+".recq"),0777)
                        except:
                            ""
                    elif os.path.exists(os.path.join(recpath,title+".mkv")):
                        try:
                            shutil.copy(os.path.join(recpath,title+".mkv"), os.path.join(movepath,title+".mkv"))
                        except:
                            ""
                        try:
                            os.chmod(os.path.join(movepath,title+".mkv"),0777)
                        except:
                            ""
                        recque.writeRecQue(movepath, chtxt, title, opt)
                        try:
                            os.chmod(os.path.join(movepath,title+".recq"),0777)
                        except:
                            ""
                except Exception, inst:
                    errtxt1="move mkv/mp4 error."
                    errtxt2=str(type(inst))+"\n"
                    errtxt2=errtxt2+str(inst)
                    recdblist.addCommonlogEX("Error", "type_encode_que(timerec.py)", errtxt1,errtxt2+traceback.format_exc(),log_level=200)
                recdb.reserveReckey(rec10const.REC_MOVE_END, title, chtxt, btime, etime, opt)
            recdb.reserveReckey(typetxtfin, title, chtxt, btime, etime, opt)
            sys.exit()
        else:
            recdb.deleteReckey(typetxtnow, title, chtxt, btime)
            bt = bt + datetime.timedelta(seconds=600)
            et = et + datetime.timedelta(seconds=600)
            btime = bt.strftime("%Y-%m-%d %H:%M:%S")
            etime = et.strftime("%Y-%m-%d %H:%M:%S")
            recdb.reserveReckey(typetxtnow, title, chtxt, btime, etime, opt)
            sys.exit()
