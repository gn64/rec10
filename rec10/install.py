#!/usr/bin/python
# coding: UTF-8
# Rec10 TS Recording Tools
# Copyright (C) 2009-2014 Yukikaze

import time
import os.path
import ConfigParser
import sys
import commands
import configwriter
import shutil
import traceback

import recdblist
import rec10const

def printutf8(str):
    print str.encode('utf-8')
    
def install():
    version = rec10const.version
    recdblist.printutf8(u"rec10の環境インストールを開始します。",verbose_level=100)
    time.sleep(1)
    recdblist.printutf8(u"環境インストール処理中・・・",verbose_level=100)
    path = str(os.path.dirname(os.path.abspath(__file__)))
    confp = ConfigParser.SafeConfigParser()
    Conf = 'rec10.conf'
    confpath=""
    if os.path.exists(os.path.join(path,Conf)):
        confpath=os.path.join(path,Conf)
    elif os.path.exists(os.path.join("/etc","rec10.conf")):
        confpath=os.path.join("/etc","rec10.conf")
    elif os.path.exists(os.path.join("/etc/rec10","rec10.conf")):
        confpath=os.path.join("/etc/rec10","rec10.conf")
    if confpath!="":
        confp.read(confpath)
    else:
        recdblist.printutf8(u"rec10.confが見つかりません。",verbose_level=100)
        recdblist.printutf8(u"このinstall.pyと同じフォルダに置くか、/etc/rec10.confもしくは/etc/rec10/rec10.confにおいてください。",verbose_level=100)
        sys.exit(1)
    recdblist.printutf8(u"設定ファイルの読み込みを確認//設定ファイルのパスは"+confpath,verbose_level=100)
    if confp.get('path',"recpath")=="/path of /recording":
        recdblist.printutf8(u"録画先のフォルダを設定してください(recpath=)",verbose_level=100)
        sys.exit(1)
    recdblist.printutf8(u"DB処理に入ります",verbose_level=100)
    if os.path.exists(os.path.join(path,"rec10d.py")):
        try:
            update_db_all()
        except Exception, inst:
            recdblist.printutf8(u"DB処理中にエラーが出ました。configファイルのDB設定を見直してください。",verbose_level=100)
            recdblist.printutf8(str(type(inst)),verbose_level=100)
            recdblist.printutf8(str(inst)+"\n"+traceback.format_exc(),verbose_level=100)
    import rec10d
    rec10d.rec10db.change_installed_in_status()
    recdblist.printutf8(u"チャンネル設定に入ります",verbose_level=100)
    import scan_ch
    rec10d.rec10db.new_epg_ch()
    recdblist.printutf8(u"チャンネルスキャンの開始",verbose_level=100)
    scan_ch.searchCh()
    rec10d.rec10db.change_installed_in_status(100)
    recdblist.printutf8(u"おめでとうございます　初期設定は完了しました。",verbose_level=100)
    recdblist.printutf8(u"rec10を実行するユーザーのcrontabに"+os.path.join(path,"rec10")+u"を追加してください(5分周期が目安)",verbose_level=100)
    sys.exit(1)
    
def getLocalPath(cmd,exit=1):
    printutf8(cmd+u"の場所を確認中")
    txt=commands.getoutput(u"which "+cmd)
    if len(txt.split(" "))>3:
        if exit==1:
            printutf8(cmd+u"の場所を確認できませんでした。終了します")
            sys.exit(1)
        else:
            return ""
    else:
        printutf8(cmd+u" : "+txt)
        if len(txt.splitlines()):
            txt=txt.splitlines()[0]
        return txt.strip()
    
def move():
    useB=0
    path = str(os.path.dirname(os.path.abspath(__file__)))
    printutf8(u"rec10 ver "+rec10const.version_str+" installer.")
    printutf8(u"rec10のインストールを開始します。")
    maxn=16
    configwriter.setTempConfPath(u"wine", getLocalPath(u"wine"))
    configwriter.setTempConfPath(u"recpt1", getLocalPath(u"recpt1",exit=0))
    #configwriter.setpath(u"x264", getLocalPath(u"x264"))
    configwriter.setTempConfPath(u"mp4box", getLocalPath(u"MP4Box",exit=0))
    configwriter.setTempConfPath(u"mencoder", getLocalPath(u"mencoder"))
    configwriter.setTempConfPath(u"ffmpeg", getLocalPath(u"ffmpeg"))
    configwriter.setTempConfPath(u"mkvmerge", getLocalPath(u"mkvmerge",exit=0))
    configwriter.setTempConfPath(u"lame", getLocalPath(u"lame",exit=0))
    configwriter.setTempConfPath(u"b25", getLocalPath(u"b25"))
    configwriter.setTempConfPath(u"tee", getLocalPath(u"tee"))
    configwriter.setTempConfPath(u"java", getLocalPath(u"java"))
    try:
        configwriter.setTempConfPath(u"lsmash", getLocalPath(u"muxer"))
        configwriter.setTempConfPath(u"useLSmash", "1")
    except:
        printutf8(u"skip L-Smash configurations.")
        ""
    try:
        configwriter.setTempConfPath(u"lame", getLocalPath(u"lame"))
    except:
        printutf8(u"lameが見つかりません(NeroAacEncを導入する場合は問題ありません)")
    printutf8(u"必須環境の設定が完了しました")
    printutf8(u"個人設定に入ります")
    if os.path.exists("/etc/rec10.conf"):
        printutf8(u"以前の設定ファイルが見つかりました。")
        printutf8(u"インポートしますか？")
        useB=raw_input("[Y/n]:")
        if useB=="N" or useB == "n":
            useB=0
        else:
            useB=1
            printutf8(u"設定ファイルのインポートを行います。")
    printutf8(u"rec10の録画ファイルが置かれる場所を入力してください(1/"+str(maxn)+")")
    printutf8(u"100GB程度は確保されている必要があります")
    recpath=getConf(useB,"/etc/rec10.conf","path","recpath","path : ")
    configwriter.setTempConfPath(u"recpath",recpath)
    printutf8(u"DBの設定に入ります。")
    printutf8(u"rec10が使用するMySQLのユーザーを設定してください。(2/"+str(maxn)+")")
    mysql_user=getConf(useB,"/etc/rec10.conf","db","mysql_user","mysql_user : ")
    configwriter.setTempConfDB("mysql_user", mysql_user)
    printutf8(u"パスワードを設定してください(3/"+str(maxn)+")")
    mysql_passwd=getConf(useB,"/etc/rec10.conf","db","mysql_passwd","mysql_passwd : ")
    configwriter.setTempConfDB("mysql_passwd", mysql_passwd)
    printutf8(u"BS/CSの設定に入ります")
    printutf8(u"BS/CS(110度CS,スカパーe2)を受信しますか(4/"+str(maxn)+")")
    useBSCS=raw_input("[Y/n]:")
    if useBSCS=="N" or useBSCS=="n" or useBSCS=="no" or useBSCS=="NO":
        useBSCS="0"
    else:
        useBSCS="1"
    printutf8(u"ドライバの設定に入ります")
    printutf8(u"DVB版のドライバを使用しますか(earth_pt1など。chardev版を使う場合はNo)(5/"+str(maxn)+")")
    useDVB=raw_input("[y/N]:")
    if useDVB=="Y" or useDVB=="y" or useDVB=="yes" or useDVB=="YES":
        useDVB="1"
    else:
        useDVB="0"
    configwriter.setTempConfDVB("useDVB", useDVB)
    if useDVB=="1":
        printutf8(u"DVBの設定に入ります")
        printutf8(u"DVBAdapterの場所を設定して下さい")
        printutf8(u"/dev/dvb/adapterなどdvr0の上のフォルダ(番号の前まで)を指定して下さい。")
        getConf(useB,"/etc/rec10.conf","dvb","DVBadapter","/dev/dvb/adapter")
        printutf8(u"地デジを受信可能なアダプタ番号を入力して下さい(1,3のように','で区切って下さい)")
        getConf(useB,"/etc/rec10.conf","dvb","DVBTE","1,3")
        if useBSCS=="1":
            printutf8(u"BS/CSを受信可能なアダプタ番号を入力して下さい(0,2のように','で区切って下さい)")
            getConf(useB,"/etc/rec10.conf","dvb","DVBBSCS","0,2")
        else:
            configwriter.setTempConfDVB("DVBBSCS","")
    printutf8(u"同時録画可能数の設定に入ります")
    printutf8(u"TE(地デジ)録画可能数(PT*だと2 白Friioだと1)(6/"+str(maxn)+")")
    te_max=str(int(getConf(useB,"/etc/rec10.conf","env","te_max","te_max : ")))
    configwriter.setTempConfEnv("te_max", te_max)
    if useBSCS=="1":
        printutf8(u"BS/CS110録画可能数(PT*だと2 黒Friioだと1)(7/"+str(maxn)+")")
        bscs_max=str(int(getConf(useB,"/etc/rec10.conf","env","bscs_max","bscs_max : ")))
        configwriter.setTempConfEnv("bscs_max", bscs_max)
    else:
        configwriter.setTempConfEnv("bscs_max", "0")
    printutf8(u"同時エンコード最大数を設定してください(8/"+str(maxn)+")")
    printutf8(u"CPUのコア数が目安です")
    enc_max=raw_input("[2]:")
    if enc_max=="":
        enc_max="2"
    else:
        enc_max=str(int(enc_max))
    configwriter.setTempConfEnv("enc_max", enc_max)
    printutf8(u"二カ国語放送/5.1ch放送の音声変換の設定です。\nNeroAACエンコーダーを使用しますか?(NeroAACEncを別途入手してtstoolsに入れてください。))")
    printutf8(u"入手先:http://www.nero.com/jpn/technologies-aac-codec.html\n (9/"+str(maxn)+")")
    audioenc=raw_input("[y/N]:  ")
    if audioenc=="y" or audioenc=="Y":
        configwriter.setTempConfPath("useNeroAAC", "1")
        aacpath=getLocalPath(u"neroAacEnc",exit=0)
        if len(aacpath)<2:
            printutf8(u"NeroAACEncのパスを指定してください")
            aacpath=getConf(useB,"/etc/rec10.conf","path","NeroAAC","[/usr/local/bin/neroAacEnc] : ")
        if aacpath.replace(" ","")=="":
            aacpath="/usr/local/bin/neroAacEnc"
        configwriter.setTempConfPath("NeroAAC",aacpath)
    else:
        configwriter.setTempConfPath("useNeroAAC", "0")
        aacpath="/usr/local/bin/neroAacEnc"
        configwriter.setTempConfPath("NeroAAC",aacpath)
        printutf8(u"lameを使用します。")
    printutf8(u"x264のパスを指定してください(10/"+str(maxn)+")")
    x264path=getConf(useB,"/etc/rec10.conf","path","x264","x264 path : ")
    if x264path.replace(" ","")=="" or not os.path.isfile(x264path):
        x264path=getLocalPath(u"x264")
    configwriter.setTempConfPath("x264",x264path)
    printutf8(u"保存する画質を教えてください(11/"+str(maxn)+")")
    printutf8(u"x264のcrfの値です。標準だと30分で800MBほどになります")
    crf=getConf(useB,"/etc/rec10.conf","env","crf","[24] : ")
    if crf.replace(" ","")=="":
        crf="24"
    configwriter.setTempConfEnv("crf", crf)
    printutf8(u"アニメーションを保存する画質を教えてください(12/"+str(maxn)+")")
    printutf8(u"x264のcrfの値です。標準だと30分で800MBほどになります")
    a_crf=getConf(useB,"/etc/rec10.conf","env","animation_crf","[20] : ")
    if a_crf.replace(" ","")=="":
        a_crf="20"
    configwriter.setTempConfEnv("animation_crf", a_crf)
    printutf8(u"x264の圧縮率を設定してください(13/"+str(maxn)+")")
    printutf8(u"x264のpresetの値です。標準だとmedium、小さければ小さいほど高圧縮になります")
    preset=getConf(useB,"/etc/rec10.conf","env","x264_preset","[4] : ")
    if preset.replace(" ","")=="":
        preset="4"
    configwriter.setTempConfEnv("x264_preset", preset)
    printutf8(u"保存するコンテナの設定(14/"+str(maxn)+")")
    printutf8(u"MP4を標準にしますか(もしくはMKVが使えます)")
    useMP4=raw_input("[Y/n]:")
    if useMP4=="N" or useMP4 == "n" :
        useMP4="0"
    else:
        useMP4="1"
    configwriter.setTempConfEnv("make_mp4", useMP4)
    printutf8(u"TSファイルの自動削除(15/"+str(maxn)+")")
    printutf8(u"中間体のtsファイルを削除しますか")
    printutf8(u"(削除するとやり直しができないため残しておくことを推奨します。)")
    removeTS=raw_input("[y/N]:")
    if removeTS=="Y" or removeTS == "y" :
        removeTS="1"
    else:
        removeTS="0"
    configwriter.setTempConfEnv("remove_ts", removeTS)
    printutf8(u"自機識別オプション(16/"+str(maxn)+")")
    printutf8(u"本rec10の識別名を入力してください")
    printutf8(u"複数台のrec10を運用する場合に効果的です。空白でもかまいません。")
    printutf8(u"例 kobe01")
    iff=getConf(useB,"/etc/rec10.conf","env","iff","[] : ")
    configwriter.setTempConfEnv("iff", iff)
    confp = ConfigParser.SafeConfigParser()
    confp.read("/etc/rec10.conf")
    if useB:
        configwriter.setTempConfPath("recorded",confp.get("path","recorded"))
        configwriter.setTempConfPath("move_destpath",confp.get("path","move_destpath"))
        configwriter.setTempConfPath("tmp",confp.get("path","tmp"))
        try:
            configwriter.setTempConfEnv("x264_addline",confp.get("env","x264_addline"))
        except:
            configwriter.setTempConfEnv("x264_addline","")
        if confp.get("path","b25_remote")=="":
            configwriter.setTempConfPath("b25_remote", "0")
        elif confp.get("path","b25_remote")=="1":
            configwriter.setTempConfPath("b25_remote", confp.get("path","b25_remote"))
            configwriter.setTempConfPath("b25", confp.get("path","b25"))
            configwriter.setTempConfPath("b25_env", confp.get("path","b25_env"))
        else:
            configwriter.setTempConfPath("b25_remote", confp.get("path","b25_remote"))

    configwriter.writeTempConf()
    printutf8(u"初期設定が終了しました。")
    printutf8(u"makeを実行した後にrootにてmake installを実行するとインストールされます。")
    
def getConf(useBeforeConf,Confpath,dbsection,key,defaultstr):
    tstr=defaultstr
    if os.path.exists(Confpath) and useBeforeConf==1:
        try:
            confp = ConfigParser.SafeConfigParser()
            confp.read("/etc/rec10.conf")
            tstr=u"["+confp.get(dbsection,key)+u"]:"
        except:
            tstr=defaultstr
    tmpconf=raw_input(tstr)
    if tmpconf.replace(" ","")=="" and os.path.exists(Confpath) and useBeforeConf==1 :
        confp = ConfigParser.SafeConfigParser()
        confp.read("/etc/rec10.conf")
        try:
            tmpconf=confp.get(dbsection,key)
        except:
            tmpconf=""
    return tmpconf

def update_db_all():
    import rec10d
    tversion=0
    if rec10d.rec10db.select_installed_in_status()==1:
        if rec10d.rec10db.select_version_in_status()==0:
            tversion=0
            recdblist.printutf8(u"既存のDBが見つかりました。0.9.1と仮定してアップデート処理を行います。",verbose_level=100)
            update_db(0)
            time.sleep(1)
        else:
            recdblist.printutf8(u"既存のDBが見つかりました。アップデート処理を行います。",verbose_level=100)
        tversion=int(rec10d.rec10db.select_version_in_status())
        while (rec10const.version>tversion):
            update_db(tversion)
            tversion=int(rec10d.rec10db.select_version_in_status())
    else:
        rec10d.rec10db.drop_in_status()
        rec10d.rec10db.drop_in_settings()
        time.sleep(2)
        rec10d.rec10db.new_in_status()
        rec10d.rec10db.new_in_settings()
        rec10d.rec10db.change_version_in_status(rec10const.version)
        
def update_db(version):
    import rec10d
    if version==0:
        rec10d.rec10db.update_db_to93()
    elif version==93:
        rec10d.rec10db.update_db_93to94()
    elif version==94:
        rec10d.rec10db.update_db_94to95()
    elif version==95:
        rec10d.rec10db.update_db_95to96()
    elif version==96:
        rec10d.rec10db.update_db_96to98()
    elif version==98:
        rec10d.rec10db.update_db_98to100()
    elif version==100:
        rec10d.rec10db.update_db_100to101()
    elif version==101:
        rec10d.rec10db.update_db_100to102()
    elif version==102:
        rec10d.rec10db.update_db_102to103()
        
if __name__ == "__main__":
    move()
