#!/usr/bin/python
# coding: UTF-8
# Rec10 TS Recording Tools
# Copyright (C) 2009-2014 Yukikaze

import datetime
import os
import os.path
import traceback
import commands

import configreader
import rec10const

os.environ['LANG'] = "ja_JP.UTF-8"
my_env = os.environ
my_env['LANG'] = "ja_JP.UTF-8"


def getEnv():
    return my_env


def printutf8(unicode, verbose_level=500):
    if rec10const.verbose_level_now > verbose_level:
        try:
            str = unicode.encode('utf-8')
            print str
        except Exception, inst:
            addCommonlogEX("Error", "printutf8(recdblist.py)", str(type(inst)), str(inst) + traceback.format_exc(),
                           log_level=200)


def printutf8ex(unicode, verbose_level=500, log_level=500):
    str = unicode.encode('utf-8')
    if rec10const.verbose_level_now > verbose_level:
        print str
    logfname = "/var/log/rec10"
    mode = "a"
    if rec10const.log_level_now > log_level:
        if os.path.exists(logfname):
            f = open(logfname, mode)
            f.write(str + u"\n")
            f.close()


def printutf8_Important(unicode):
    printutf8ex(unicode, 100, 100)


def printutf8_Normal(unicode):
    printutf8ex(unicode, 300, 300)


def printutf8_Detailed(unicode):
    printutf8ex(unicode, 600, 600)


def addCommonlogEX(type, place, inst, txt, verbose_level=500, log_level=500):
    lt = unicode(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")) + u":"
    if txt == "":
        lt = lt + u"[" + type + u"] " + place + u" " + inst
    else:
        try:
            lt = lt + u"[" + type + u"] " + place + u" " + inst + u"\n" + txt
        except:
            print [type, place, inst, txt]
    lt = lt.encode('utf-8')
    logfname = "/var/log/rec10"
    mode = "a"
    if rec10const.verbose_level_now > verbose_level:
        print lt
    if rec10const.log_level_now > log_level:
        if os.path.exists(logfname):
            f = open(logfname, mode)
            f.write(lt + "\n")
            f.close()


def addCommandLog(tspath, log_title, cmd, cmd_log=""):
    addLog(tspath, cmd, log_title + u"ログ-コマンド")
    addLog(tspath, cmd_log, log_title + u"ログ-詳細")


def addCommandLogZip(tspath, log_title, log_filename_add, cmd, cmd_log=""):
    logtitle = getLogTitle(tspath)
    logo = logtitle + "." + log_filename_add + ".log"
    logzip = logtitle + ".log.zip"
    addLogAll(tspath, cmd + u"\n\n" + cmd_log, log_title, logo)
    import zip

    zip.addFile2FileZip(logo, logzip)
    os.remove(logo)


def addCommandSelfLog(tspath, command):
    logpath = getLogTitle(tspath) + ".command.log"
    f = ""
    if os.path.exists(logpath):
        f = open(logpath, 'a')
    else:
        f = open(logpath, 'w')
    tc = command + u"\n\n"
    tc = unicode(tc)
    f.write(tc.encode("utf-8"))
    # f.write(tc.encode('utf-8','ignore'))
    f.close()


def getLogTitle(tspath):
    logo = tspath
    # auto_process deleteTmpFileも参照のこと
    ext_levels = rec10const.EXT_LEVEL_0 + rec10const.EXT_LEVEL_4 + rec10const.EXT_LEVEL_9 + rec10const.EXT_LEVEL_10
    for pext in ext_levels:
        logo = logo.replace(pext, u"")
    return logo


def addLog(tspath, txt, log_title, maxtextlength=4000):
    logo = getLogTitle(tspath) + ".log"
    f = open(logo, 'a')
    s = len(txt)
    stxt = ""
    if s > maxtextlength * 2 and maxtextlength > 0:
        stxt = txt[0:maxtextlength] + u"\n\n(ry..)\n" + txt[s - maxtextlength:]
    else:
        stxt = txt
    txtw = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    txtw = txtw + u"\n####" + log_title + u"####\n" + stxt
    f.write(txtw.encode('utf-8'))
    f.close()


def addLogAll(tspath, txt, log_title, logpath):
    logo = logpath
    f = open(logo, 'a')
    stxt = txt
    txtw = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    txtw = txtw + u"\n####" + log_title + u"####\n" + stxt
    f.write(txtw.encode('utf-8'))
    f.close()


def execCommand(cmd, pout, Log_title, Ziptitle):
    printutf8(cmd)
    addLog(pout, cmd, Log_title + u" command")
    try:
        txt = unicode(commands.getoutput(cmd.encode('utf-8')), "utf-8") + "\n"
    except Exception, inst:
        txt = u"error occures in execCommand\n"
        txt = txt + str(type(inst)) + "\n"
        txt = txt + str(inst)
    addLog(pout, txt, Log_title + u" details")
    addCommandSelfLog(pout, cmd)
    addCommandLogZip(pout, Log_title, Log_title, cmd, txt)
