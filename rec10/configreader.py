#!/usr/bin/python
# coding: UTF-8
# Rec10 TS Recording Tools
# Copyright (C) 2009-2014 Yukikaze
import ConfigParser
import os
import os.path
mypath = str(os.path.dirname(os.path.abspath(__file__)))
confp = ConfigParser.SafeConfigParser()
Conf = 'rec10.conf'
confpath=""
if os.path.exists(os.path.join(mypath,Conf)):
    confpath=os.path.join(mypath,Conf)
elif os.path.exists(os.path.join("/usr/local/etc","rec10.conf")):
    confpath=os.path.join("/usr/local/etc","rec10.conf")
elif os.path.exists(os.path.join("/usr/local/etc/rec10","rec10.conf")):
    confpath=os.path.join("/usr/local/etc/rec10","rec10.conf")
elif os.path.exists(os.path.join("/etc","rec10.conf")):
    confpath=os.path.join("/etc","rec10.conf")
elif os.path.exists(os.path.join("/etc/rec10","rec10.conf")):
    confpath=os.path.join("/etc/rec10","rec10.conf")
confp.read(confpath)

def getConfPath(string):
    global confp
    return confp.get('path', string)

def getConfDB(string):
    global confp
    return confp.get('db', string)

def getConfEnv(string):
    global confp
    return confp.get('env', string)

def getConfLog(string):
    global confp
    return confp.get('log', string)

def getConfDVB(string):
    global confp
    return confp.get('dvb',string)
