// CasProxy.cpp: CCasProxy クラスのインプリメンテーション
//
//////////////////////////////////////////////////////////////////////


#include "CasProxy.h"
#include <stdio.h>

#define TCP_TIMEOUT	1000UL	// 1秒


DWORD CCasProxy::dwErrorDelayTime = 0UL;


CCasProxy::CCasProxy(void)
{

}

CCasProxy::~CCasProxy(void)
{
	// サーバから切断
	m_Socket.Close();
}

const BOOL CCasProxy::Connect(void)
{
/*
	// エラー発生時のガードインターバル
	if(dwErrorDelayTime){
		if((::GetTickCount() - dwErrorDelayTime) < TCP_TIMEOUT)return FALSE;
		else dwErrorDelayTime = 0UL;
		}
*/
	// サーバに接続
	char* env = getenv("B25_SERVER_IP");
	LPCTSTR lpszHost;
	WORD wPort;
	if (env) {
		lpszHost = env;
	}
	else {
		lpszHost = "127.0.0.1";
	}
	env = getenv("B25_SERVER_PORT");
	if (env) {
		wPort = atoi(env);
	}
	else {
		wPort = 6900;
	}

	if(m_Socket.Connect(lpszHost, wPort, TCP_TIMEOUT)){
		return TRUE;
	}
	else{
		//dwErrorDelayTime = ::GetTickCount();
		return FALSE;
	}
}

const DWORD CCasProxy::TransmitCommand(const BYTE *pSendData, const DWORD dwSendSize, BYTE *pRecvData)
{
	// 送信データ準備
	BYTE SendBuf[256];
	SendBuf[0] = (BYTE)dwSendSize;
	memcpy(&SendBuf[1], pSendData, dwSendSize);

	try{
		// リクエスト送信
		if(!m_Socket.Send(SendBuf, dwSendSize + 1UL, TCP_TIMEOUT))throw (const DWORD)__LINE__;
	
		// レスポンスヘッダ受信
		if(!m_Socket.Recv(SendBuf, 1UL, TCP_TIMEOUT))throw (const DWORD)__LINE__;

		// レスポンスデータ受信
		if(!m_Socket.Recv(pRecvData, SendBuf[0], TCP_TIMEOUT))throw (const DWORD)__LINE__;
		}
	catch(const DWORD dwLine){
		// 通信エラー発生
		m_Socket.Close();
		return 0UL;
		}
		
	return SendBuf[0];
}

