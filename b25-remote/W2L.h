#include <string.h>

#define TCHAR char
#define PVOID void*
#define HANDLE void*
#define TRUE 1
#define FALSE 0
#define INVALID_SOCKET (-1)
#define SOCKET_ERROR (-1)
#define SOCKADDR_IN struct sockaddr_in
#define SOCKADDR struct sockaddr
#define PSOCKADDR SOCKADDR*
#define SD_BOTH SHUT_RDWR

