#include <stdio.h>
#include "CasServer.h"

CCasServer m_CasServer(NULL);
int m_dwServerPort = 6900UL;

int main() {
	if(!m_CasServer.OpenServer(m_dwServerPort)){
		printf("サーバの起動に失敗しました。\nTCPポートまたはカードリーダをオープンできません。\n");
		return TRUE;
	}
	getchar();
}

